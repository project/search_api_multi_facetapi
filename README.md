Overview
========

Enables search facets for multi-index searches.

Installation
============

Download the module and enable it as you would any other Drupal module.

Usage
=====

Since the search index is defined per server for multi-index searches, the configuration for the facets is under the search api server section.

Go to admin/config/search/search_api and edit a compatible search server (e.g. Solr). You'll see a new tab called "Facets" where you configure the facet fields used by this server.

Once set, a series of blocks will be made available for each facet you've defined.

